﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TodoAppAPI.Models;
using TodoAppAPI.DataAccess;

namespace TodoAppAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TodoItemsController : ControllerBase
    {
        private readonly IDataAccessProvider _dataAccessProvider;

        public TodoItemsController(IDataAccessProvider dataAccessProvider)
        {
            _dataAccessProvider = dataAccessProvider;
        }

        // GET: api/TodoItems
        [HttpGet]
        public IEnumerable<TodoItem> GetTodoItems()
        {
            return _dataAccessProvider.GetTodoItems();
        }

        // GET: api/TodoItems/5
        [HttpGet("{id}")]
        public TodoItem GetTodoItem(int id)
        {
            return _dataAccessProvider.GetTodoItemSingle(id);
        }

        // PUT: api/TodoItems/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public IActionResult UpdateTodoItem(TodoItem todoItem)
        {
            if (ModelState.IsValid)
            {
                _dataAccessProvider.UpdateTodoItem(todoItem);
                return Ok();
            }
            return BadRequest();
        }

        // POST: api/TodoItems
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public IActionResult CreateTodoItem(TodoItem todoItem)
        {
            if (ModelState.IsValid)
            {
                _dataAccessProvider.AddTodoItem(todoItem);
                return Ok();
            }
            return BadRequest();
        }
        // DELETE: api/TodoItems/5
        [HttpDelete("{id}")]
        public IActionResult DeleteTodoItem(int id)
        {
            var data = _dataAccessProvider.GetTodoItemSingle(id);
            if (data == null)
            {
                return NotFound();
            }
            _dataAccessProvider.DeleteTodoItem(id);
            return Ok();
        }


        //private static TodoItemDTO ItemToDTO(TodoItem todoItem) =>
        //new TodoItemDTO
        //{
        //    Id = todoItem.Id,
        //    Name = todoItem.Name,
        //    IsComplete = todoItem.IsComplete
        //};
    }
}
