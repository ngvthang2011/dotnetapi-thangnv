﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TodoAppAPI.Models;

namespace TodoAppAPI.DataAccess
{
    public class DataAccessProvider: IDataAccessProvider
    {
        private readonly PostgreSqlContext _context;

        public DataAccessProvider(PostgreSqlContext context)
        {
            _context = context;
        }

        public void AddTodoItem(TodoItem todoItem)
        {
            _context.todos.Add(todoItem);
            _context.SaveChanges();
        }

        public void UpdateTodoItem(TodoItem todoItem)
        {
            _context.todos.Update(todoItem);
            _context.SaveChanges();
        }

        public void DeleteTodoItem(int id)
        {
            var entity = _context.todos.FirstOrDefault(t => t.id == id);
            _context.todos.Remove(entity);
            _context.SaveChanges();
        }

        public TodoItem GetTodoItemSingle(int id)
        {
            return _context.todos.FirstOrDefault(t => t.id == id);
        }

        public List<TodoItem> GetTodoItems()
        {
            return _context.todos.ToList();
        }
    }
}
