﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TodoAppAPI.Models;

namespace TodoAppAPI.DataAccess
{
    public interface IDataAccessProvider
    {
        void AddTodoItem(TodoItem todoItem);
        void UpdateTodoItem(TodoItem todoItem);
        void DeleteTodoItem(int id);
        TodoItem GetTodoItemSingle(int id);
        List<TodoItem> GetTodoItems();
    }
}
