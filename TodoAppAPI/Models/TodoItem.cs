﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TodoAppAPI.Models
{
    public class TodoItem
    {
        public int id { get; set; }
        public string name { get; set; }
        public bool iscomplete { get; set; }
        public string secret { get; set; }
    }
}
