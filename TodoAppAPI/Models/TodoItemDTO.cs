﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TodoAppAPI.Models
{
    public class TodoItemDTO
    {
        public long id { get; set; }
        public string name { get; set; }
        public bool iscomplete { get; set; }

    }
}
